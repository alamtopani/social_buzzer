class CreateBuzzerProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :buzzer_profiles do |t|
      t.string :first_name
      t.string :last_name
      t.date :born
      t.string :gender
      t.string :from
      t.boolean :agreement
      t.integer :buzzer_id
      t.text :industries, array: true, default: []
      t.string :work

      t.timestamps
    end

    add_index :buzzer_profiles, :buzzer_id
  end
end
