class CreateAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :addresses do |t|
      t.integer :user_id
      t.string :country
      t.string :province
      t.string :city
      t.text :address
      t.string :longitude
      t.string :latitude

      t.timestamps
    end

    add_index :addresses, :user_id
  end
end
