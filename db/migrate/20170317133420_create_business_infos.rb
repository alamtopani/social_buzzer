class CreateBusinessInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :business_infos do |t|
      t.string :name
      t.string :website
      t.string :admin_name
      t.string :phone_admin
      t.text :about
      t.string :from
      t.boolean :agreement
      t.integer :business_id
      t.text :industries, array: true, default: []

      t.timestamps
    end

    add_index :business_infos, :business_id
  end
end
