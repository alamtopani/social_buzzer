class CreateVisitorSnapshots < ActiveRecord::Migration[5.0]
  def change
    create_table :visitor_snapshots do |t|
      t.string :visitorable_type
      t.string :visitorable_id

      t.timestamps
    end

    add_index :visitor_snapshots, :visitorable_id
  end
end
