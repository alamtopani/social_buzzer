# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170701013442) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "country"
    t.string   "province"
    t.string   "city"
    t.text     "address"
    t.string   "longitude"
    t.string   "latitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_addresses_on_user_id", using: :btree
  end

  create_table "business_infos", force: :cascade do |t|
    t.string   "name"
    t.string   "website"
    t.string   "admin_name"
    t.string   "phone_admin"
    t.text     "about"
    t.string   "from"
    t.boolean  "agreement"
    t.integer  "business_id"
    t.text     "industries",  default: [],              array: true
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["business_id"], name: "index_business_infos_on_business_id", using: :btree
  end

  create_table "buzzer_profiles", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.date     "born"
    t.string   "gender"
    t.string   "from"
    t.boolean  "agreement"
    t.integer  "buzzer_id"
    t.text     "industries", default: [],              array: true
    t.string   "work"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["buzzer_id"], name: "index_buzzer_profiles_on_buzzer_id", using: :btree
  end

  create_table "impressions", force: :cascade do |t|
    t.string   "impressionable_type"
    t.integer  "impressionable_id"
    t.integer  "user_id"
    t.string   "controller_name"
    t.string   "action_name"
    t.string   "view_name"
    t.string   "request_hash"
    t.string   "ip_address"
    t.string   "session_hash"
    t.text     "message"
    t.text     "referrer"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.text     "params"
    t.index ["controller_name", "action_name", "ip_address"], name: "controlleraction_ip_index", using: :btree
    t.index ["controller_name", "action_name", "request_hash"], name: "controlleraction_request_index", using: :btree
    t.index ["controller_name", "action_name", "session_hash"], name: "controlleraction_session_index", using: :btree
    t.index ["impressionable_type", "impressionable_id", "ip_address"], name: "poly_ip_index", using: :btree
    t.index ["impressionable_type", "impressionable_id", "params"], name: "poly_params_request_index", using: :btree
    t.index ["impressionable_type", "impressionable_id", "request_hash"], name: "poly_request_index", using: :btree
    t.index ["impressionable_type", "impressionable_id", "session_hash"], name: "poly_session_index", using: :btree
    t.index ["impressionable_type", "message", "impressionable_id"], name: "impressionable_type_message_index", using: :btree
    t.index ["user_id"], name: "index_impressions_on_user_id", using: :btree
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.string   "taggable_type"
    t.integer  "taggable_id"
    t.string   "tagger_type"
    t.integer  "tagger_id"
    t.string   "context",       limit: 128
    t.datetime "created_at"
    t.index ["context"], name: "index_taggings_on_context", using: :btree
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
    t.index ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
    t.index ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy", using: :btree
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id", using: :btree
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type", using: :btree
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type", using: :btree
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id", using: :btree
  end

  create_table "tags", force: :cascade do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
    t.index ["name"], name: "index_tags_on_name", unique: true, using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                             default: "",    null: false
    t.string   "encrypted_password",                default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",                   default: 0,     null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "username"
    t.string   "provider"
    t.string   "uid"
    t.string   "slug"
    t.boolean  "featured",                          default: false
    t.boolean  "verified",                          default: false
    t.string   "type"
    t.string   "twitter_uid"
    t.string   "twitter_token"
    t.string   "twitter_secret"
    t.datetime "twitter_oauth_token_register_at"
    t.integer  "twitter_followers_count"
    t.string   "twitter_avatar"
    t.string   "facebook_uid"
    t.string   "facebook_oauth_token"
    t.string   "facebook_oauth_token_expires"
    t.datetime "facebook_oauth_token_register_at"
    t.integer  "facebook_friends_count"
    t.string   "facebook_avatar"
    t.string   "instagram_uid"
    t.string   "instagram_oauth_token"
    t.string   "instagram_oauth_token_expires"
    t.datetime "instagram_oauth_token_register_at"
    t.integer  "instagram_followers_count"
    t.string   "instagram_avatar"
    t.string   "google_uid"
    t.string   "google_oauth_token"
    t.string   "google_oauth_token_expires"
    t.datetime "google_oauth_token_register_at"
    t.integer  "google_followers_count"
    t.string   "google_avatar"
    t.string   "youtube_uid"
    t.string   "youtube_oauth_token"
    t.string   "youtube_oauth_token_expires"
    t.datetime "youtube_oauth_token_register_at"
    t.integer  "youtube_followers_count"
    t.string   "youtube_avatar"
    t.string   "phone"
    t.string   "pin"
    t.string   "referral_code"
    t.string   "avatar"
    t.decimal  "money",                             default: "0.0"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree
  end

  create_table "visitor_snapshots", force: :cascade do |t|
    t.string   "visitorable_type"
    t.string   "visitorable_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["visitorable_id"], name: "index_visitor_snapshots_on_visitorable_id", using: :btree
  end

end
