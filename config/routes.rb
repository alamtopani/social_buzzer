Rails.application.routes.draw do
  devise_for :users,
    controllers: {
      omniauth_callbacks: 'omniauth_callbacks',
      registrations: 'registrations',
      sessions: 'sessions'
    },
    path_names: {
      sign_in:  'login',
      sign_out: 'logout',
      sign_up:  'register'
    }

  root "home#index"

  namespace :buzzer do
    get 'dashboard', to: 'home#dashboard', as: 'dashboard'
    get 'jobs', to: 'home#jobs', as: 'jobs'
    get 'job/:id', to: 'home#job_show', as: 'job_show'
    resources :accounts
    resources :activities
    resources :transactions
    resources :credits
    resources :buzzer_shares
  end

  namespace :business do
    get 'dashboard', to: 'home#dashboard', as: 'dashboard'
    resources :billings
    resources :accounts
    resources :promotions
    resources :vouchers
  end

  resources :xhrs do
    collection do
      get :cities
    end
  end
end
