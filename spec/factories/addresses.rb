FactoryGirl.define do
  factory :address do
    user_id 1
    country "MyString"
    province "MyString"
    city "MyString"
    address "MyText"
    longitude "MyString"
    latitude "MyString"
  end
end
