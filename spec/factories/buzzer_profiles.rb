FactoryGirl.define do
  factory :buzzer_profile do
    first_name "MyString"
    last_name "MyString"
    born "2017-03-17"
    gender "MyString"
    country "MyString"
    province "MyString"
    city "MyString"
    address "MyText"
    from "MyString"
    agreement false
    age 1
    buzzer_id 1
  end
end
