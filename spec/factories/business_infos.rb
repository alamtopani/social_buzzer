FactoryGirl.define do
  factory :business_info do
    name "MyString"
    website "MyString"
    admin_name "MyString"
    phone_admin "MyString"
    about "MyText"
    from "MyString"
    agreement false
    business_id 1
  end
end
