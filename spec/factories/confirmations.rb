FactoryGirl.define do
  factory :confirmation do
    user_id 1
    invoice_code "MyString"
    name "MyString"
    email "MyString"
    payment_date "2017-07-12"
    price 1
    bank_account "MyString"
    payment_method "MyString"
    sender_name "MyString"
    message "MyString"
    category "MyString"
    status "MyString"
  end
end
