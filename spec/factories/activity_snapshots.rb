FactoryGirl.define do
  factory :activity_snapshot do
    title "MyString"
    activitiable_type "MyString"
    activitiable_id "MyString"
    user_id 1
    code "MyString"
  end
end
