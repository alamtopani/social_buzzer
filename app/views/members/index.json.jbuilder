json.members @members do |m|
  json.(m, :id, :uid, :username, :email, :type, :avatar_url, :created_at, :updated_at, :last_sign_in_at, :current_sign_in_at, :sign_in_count)

  json.buzzer_profile do
    json.(m.buzzer_profile, :first_name, :last_name, :born, :gender, :from)
    json.(m.buzzer_profile, :industries)
  end

  json.address do
    json.(m.address, :country, :province, :city, :address)
    json.(m.address, :longitude, :latitude)
  end
end