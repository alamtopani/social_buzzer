module CurrencyHelper
  def get_currency(number)
    number = 0 if number.blank?
    number_to_currency(number, :unit => "Rp ", :separator => ",", :delimiter => ".", precision: 0)
  end

  def get_nominal(number)
    number = 0 if number.blank?
    number_to_currency(number, :unit => "", :separator => ",", :delimiter => ".", precision: 0)
  end
end