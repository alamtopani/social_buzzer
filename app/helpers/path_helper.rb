module PathHelper
  def dashboard(user)
    if user.is_business?
      return business_dashboard_path
    elsif user.is_buzzer?
      return buzzer_dashboard_path
    end
  end

  def edit_account(user)
    if user.is_business?
      return edit_business_account_path(user)
    elsif user.is_buzzer?
      return edit_buzzer_account_path(user)
    end
  end

  def active_sidebar?(controller, *actions)
    if actions.include?(params[:action].to_sym) || actions.include?(:all)
      return 'active'
    end if controller_name == controller.to_s
  end
end