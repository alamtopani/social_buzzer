module ApplicationHelper
  def format_date(date)
    date.strftime('%d %b %Y') if date.present?
  end
  
  def preloader(class_name=nil)
    "<div class='spinner #{class_name}'>
      <div class='rect1'></div>
      <div class='rect2'></div>
      <div class='rect3'></div>
      <div class='rect4'></div>
      <div class='rect5'></div>
    </div>".html_safe
  end

  def preloader_sm(class_name=nil)
    "<div class='spinner-small #{class_name}'>
      <div class='rect1'></div>
      <div class='rect2'></div>
      <div class='rect3'></div>
      <div class='rect4'></div>
      <div class='rect5'></div>
    </div>".html_safe
  end

  def splash_screen(class_name=nil)
    "<div class='splash #{class_name}'>
      <div class='color-line'></div>
      <div class='splash-title'>
        <div class='spinner'>
          <div class='rect1'></div>
          <div class='rect2'></div>
          <div class='rect3'></div>
          <div class='rect4'></div>
          <div class='rect5'></div>
        </div>
      </div>
    </div>".html_safe
  end
  
  def class_for flash_type
    { success: "alert-success", error: "alert-danger", errors: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym] || flash_type.to_s
  end

  def flash_messages(opts = {})
    if flash.present?
      content_tag(:div, class: 'section-alert') do
        flash.each do |msg_type, message|
          concat(content_tag(:div, message, class: "alert #{class_for(msg_type)} alert-dismissible", role: 'alert') do
            concat(content_tag(:button, class: 'close', data: { dismiss: 'alert' }) do
              concat content_tag(:span, '&times;'.html_safe, 'aria-hidden' => true)
              concat content_tag(:span, 'Close', class: 'sr-only')
            end)
            if flash[:errors].present?
              message.each do |word|
                concat "<div class='alert alert-danger alert-dismissible'>#{word}</div>".html_safe
              end
            else
              concat "#{message}".html_safe
            end
          end)
        end
        nil
      end
    end
  end

  def get_we_are_from
    [
      ["Teman", "Teman"], 
      ["Sosial Media", "Sosial Media"], 
      ["Media Cetak (Flyer, brosur, dll)", "Media Cetak (Flyer, brosur, dll)"], 
      ["Media Online (artikel online)", "Media Online (artikel online)"], 
      ["Pameran", "Pameran"], 
      ["Lain-lain", "Lain-lain"]
    ]
  end

  def get_gender
    [["Laki-Laki", "Laki-Laki"], ["Perempuan", "Perempuan"]]
  end

  def get_industries
    [
      ['Donation', 'Donation'],
      ['Education', 'Education'],
      ['Fashion','Fashion'],
      ['Film', 'Film'],
      ['Jasa', 'Jasa'],
      ['Food', 'Food'],
      ['Politik', 'Politik'],
      ['Technology','Technology'],
      ['Travel', 'Travel'],
    ]
  end

  def get_min_friends(social)
    case social
    when 'facebook'
      return "Minimal Mempunyai #{Rails.application.secrets.fb_friends_min} Teman"
    when 'twitter'
      return "Minimal Mempunyai #{Rails.application.secrets.tw_followers_min} Followers"
    when 'instagram'
      return "Minimal Mempunyai #{Rails.application.secrets.in_followers_min} Followers"
    end
  end

  def get_package_buzzers_value(package)
    case package
    when Promotion::BASIC then package = Rails.application.secrets.basic
    when Promotion::STANDAR then package = Rails.application.secrets.standar
    when Promotion::PREMIUM  then package = Rails.application.secrets.premium
    when Promotion::PRESTIGE then package = Rails.application.secrets.prestige
    end
    return package
  end

  def time_to_ago(time)
    a = (Time.zone.now - time).to_i

    case a
    when 0 then 'baru saja'
      when 1 then '1 detik yang lalu'
      when 2..59 then a.to_s+' 2 detik yang lalu' 
      when 60..119 then '1 menit yang lalu' #120 = 2 minutes
      when 120..3540 then (a/60).to_i.to_s+' menit yang lalu'
      when 3541..7100 then '1 jam yang lalu' # 3600 = 1 hour
      when 7101..82800 then ((a+99)/3600).to_i.to_s+' jam yang lalu' 
      when 82801..172000 then '1 hari yang lalu' # 86400 = 1 day
      when 172001..518400 then ((a+800)/(60*60*24)).to_i.to_s+' hari yang lalu'
      when 518400..1036800 then '1 minggu yang lalu'
      else ((a+180000)/(60*60*24*7)).to_i.to_s+' minggu yang lalu'

      # IN ENGLISH
      # when 0 then 'just now'
      # when 1 then 'a second ago'
      # when 2..59 then a.to_s+' seconds ago' 
      # when 60..119 then 'a minute ago' #120 = 2 minutes
      # when 120..3540 then (a/60).to_i.to_s+' minutes ago'
      # when 3541..7100 then 'an hour ago' # 3600 = 1 hour
      # when 7101..82800 then ((a+99)/3600).to_i.to_s+' hours ago' 
      # when 82801..172000 then 'a day ago' # 86400 = 1 day
      # when 172001..518400 then ((a+800)/(60*60*24)).to_i.to_s+' days ago'
      # when 518400..1036800 then 'a week ago'
      # else ((a+180000)/(60*60*24*7)).to_i.to_s+' weeks ago'
    end
  end

end
