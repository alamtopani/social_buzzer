class Business::BillingsController < Business::BusinessApplicationController
  def index
    @billings = Promotion.order("id desc").page(params[:page]).per(10)
  end

  def show
    @billing = Promotion.find_by(id: params[:id])
  end
end