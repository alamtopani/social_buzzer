class Business::PromotionsController < Business::BusinessApplicationController
  before_action :resource, only: [:edit, :update, :destroy, :show]

  def index
    @promotions = Promotion.order("id desc").page(params[:page]).per(10)
  end

  def show
    @promo = Promotion.find(params[:id])
    @visit = @promo.visitor_snapshot
    @visit_count = @visit.visitor_per_year.group_by_day(:created_at, format: "%d/%m/%y").count
    
    # count shares
    shares = @promo.buzzer_shares
    @shares = shares

    # count all share buzzer profile
    @get_buzzers = BuzzerProfile.where(buzzer_id: @shares.pluck(:buzzer_id))

    if @get_buzzers.present?
      # count by age
      @max_years = Time.now.year - @get_buzzers.where.not(born: nil).pluck(:born).min.year
      @min_years = Time.now.year - @get_buzzers.where.not(born: nil).pluck(:born).max.year

      # count by gender
      @gender = @get_buzzers.group(:gender).count

      # count share facebook
      @fb_shares = shares.where(provider: 'facebook')
      @fb_range_share = Buzzer.where(id: @fb_shares.pluck(:buzzer_id)).sum(:facebook_friends_count)

      # count share twitter
      @tw_shares = shares.where(provider: 'twitter')
      @tw_range_share = Buzzer.where(id: @tw_shares.pluck(:buzzer_id)).sum(:twitter_followers_count)

      # count share map location
      get_addresses_by_user = Address.where(user_id: shares.pluck(:buzzer_id).uniq)
      @province_locations = get_addresses_by_user.group(:province).count
      @locations = get_addresses_by_user.pluck(:city).uniq
    end
  end

  def new
    @promo = Promotion.new
  end

  def create
    @promo = Promotion.new(params_promotions)
    prepare_user
    prepare_min_max_age
    prepare_pricing
    if @promo.save
      VisitorSnapshot.create(visitorable_type: @promo.class.name, visitorable_id: @promo.id.to_s)
      redirect_to business_promotions_path
    else
      redirect_to business_promotions_path
    end
  end

  def edit
  end

  def update
    prepare_user
    prepare_min_max_age
    prepare_filter_blank(params)
    if @promo.update(params_promotions)
      redirect_to business_promotions_path
    else
      redirect_to business_promotions_path
    end
  end

  def destroy
    if @promo.destroy
      redirect_to business_promotions_path
    else
      redirect_to business_promotions_path
    end 
  end

  private
    def resource
      @promo = Promotion.find params[:id]
    end

    def params_promotions
      params.require(:promotion).permit(
                                        :title, 
                                        :cover, 
                                        :description,
                                        :content, 
                                        :quota, 
                                        :start_date, 
                                        :expired_date, 
                                        :range_age, 
                                        :min_age, 
                                        :max_age, 
                                        :gender, 
                                        :created_at, 
                                        :updated_at,
                                        :address,
                                        :longitude,
                                        :latitude,
                                        :url_facebook,
                                        :url_twitter,
                                        :url_instagram,
                                        :phone,
                                        url: [], mention: [], hashtag: [], cities: [], industries: [], providers: []
                                      )
    end

    def prepare_filter_blank(params)
      @promo.url = params[:promotion][:url].reject(&:empty?)
      @promo.hashtag = params[:promotion][:hashtag].reject(&:empty?)
      @promo.mention = params[:promotion][:mention].reject(&:empty?)
      @promo.cities = params[:promotion][:cities].reject(&:empty?)
      @promo.industries = params[:promotion][:industries].reject(&:empty?)
      @promo.providers = params[:promotion][:providers].reject(&:empty?)
    end

    def prepare_user
      @promo.user_id = current_user.id
    end

    def prepare_pricing
      @promo.price = 200 * params[:promotion][:quota].to_i
      @promo.fee = @promo.price * 40/100
      @promo.total = @promo.price + @promo.fee
    end

    def prepare_min_max_age
      @promo.min_age = @promo.range_age.split(',')[0]
      @promo.max_age = @promo.range_age.split(',')[1]
    end
end




