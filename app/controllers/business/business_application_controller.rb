class Business::BusinessApplicationController < ApplicationController
  layout 'dashboard'

  before_action :get_analytics

  def get_analytics
    @get_analytics = Promotion.order("id desc").limit(5)
  end
end