class Business::AccountsController < Business::BusinessApplicationController
  include PathHelper
  prepend_before_filter :draw_password, only: :update
  
  def edit
    @account = Business.find current_user
  end

  def update
    @account = Business.find current_user
    if @account.update(params_permit)
      redirect_to edit_account(current_user), notice: 'Successfully updated!'
    else
      redirect_to edit_account(current_user), alert: @account.errors.full_messages.join('<br/>')
    end
  end

  private
    def params_permit
      params.require(:business).permit(:id, :username, :email, :phone, :pin, :avatar, :password, :password_confirmation,
                                      business_info_attributes: [:name, :website, :admin_name, :phone_admin, :about, :from, :agreement, industries: []],
                                      address_attributes: [:country, :province, :city, :address, :longitude, :latitude]
                                    )
    end

    def draw_password
      %w(password password_confirmation).each do |attr|
        params[:business].delete(attr)
      end if params[:business] && params[:business][:password].blank?
    end
end