class XhrsController < ApplicationController
  include RegionHelper

  def cities
    @cities = get_city(params[:id])

    render layout: false
  end
end