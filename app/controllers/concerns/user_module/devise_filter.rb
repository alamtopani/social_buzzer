module UserModule
  module DeviseFilter
    extend ActiveSupport::Concern

    included do
      after_filter :store_location
    end

    def store_location
      return if !request.get? || request.xhr?
      return if request.fullpath.match("/users")
      if request.format == "text/html" || request.content_type == "text/html" || request.format == 'mobile' 
        session[:previous_url] = request.fullpath
        session[:last_request_time] = Time.now.utc.to_i
      end
    end

    def after_sign_in_path_for(resource)
      if resource.current_sign_in_at == resource.last_sign_in_at
        if resource.is_buzzer?
          edit_buzzer_account_path(resource)
        elsif resource.is_business?
          edit_business_account_path(resource)
        else
          return root_path
        end
      else
        if resource.is_buzzer?
          buzzer_dashboard_path
        elsif resource.is_business?
          business_dashboard_path
        else
          session[:previous_url] || root_path
        end
      end
    end

    def authenticate_this_user!
      unless current_user.present?
        redirect_to root_path, alert: "Can't Access this page"
      end 
    end

    def after_sign_out_path_for(resource_or_scope)
      root_url
    end

  end
end