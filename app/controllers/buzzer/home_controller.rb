class Buzzer::HomeController < Buzzer::BuzzerApplicationController
  def dashboard
    @activities = ActivitySnapshot.statuses.order("id desc").page(params[:page]).per(10)
  end

  def jobs
    @promotions = Promotion.all_active.order("id desc").page(params[:page]).per(10)
  end

  def job_show
    @promotion = Promotion.all_active.find_by(id: params[:id])
    
    # count visit
    if @promotion.visitor_snapshot.present?
      @visit = @promotion.visitor_snapshot
      impressionist(@visit)
    end
  end
end