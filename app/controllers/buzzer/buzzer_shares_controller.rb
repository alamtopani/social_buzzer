class Buzzer::BuzzerSharesController < Buzzer::BuzzerApplicationController
  include ActionView::Helpers::NumberHelper
  include CurrencyHelper

  # POST STATUS WITH POPUP
  def create
    unless check_available(params[:promotion_id], params[:provider]).present?
      if check_quota(params[:promotion_id]).present?
        if params[:provider] == 'twitter'
          @buzzer_share = post_tweet(params[:tweet]) 
          post_link = params[:tweet][:twit_url]
        elsif params[:provider] == 'facebook'
          post_link = params[:link]
        end

        @buzzer_share = BuzzerShare.create(
          post_link: post_link,
          buzzer_id: current_user.id,
          promotion_id: params[:promotion_id],
          post_id: params[:provider] == 'twitter' ? @tweet_obj.id : params[:post_id],
          provider: params[:provider]
        )
        
        if @buzzer_share.present?
          save_activity(@buzzer_share)
          render json: {
            success: true,
            message: "Berhasil, promosi/iklan ini sudah anda share di #{params[:provider]} anda!"
          }
        end
      else
        render json: {
          success:false,
          message: "Gagal, kuota buzzer untuk promosi/iklan ini sudah melewati batas yang telah ditentukan, silahkan pilih yang lain!"
        }
      end
    else
      render json: {
        success:false,
        message: "Gagal, anda sudah pernah share promosi/iklan ini menggunakan #{params[:provider]}!"
      }
    end
  end

  private
    def save_activity(share)
      # Update Promotion
      promotion = share.promotion
      promotion.quota_share = promotion.quota?
      promotion.save

      # Update Status Share
      share.update(status: BuzzerShare::SUCCESS)

      # Add Activity
      activity = ActivitySnapshot.create(
        title: "Telah berhasil, share promosi/iklan (<b>#{share.promotion.title}</b>) via <b>#{share.provider}</b> dengan reward <b>Rp.#{share.reward}</b>", 
        activitiable_type: "#{share.class.name}", activitiable_id: "#{share.id}", user_id: current_user.id
      )

      # Update User Money
      current_user.update(money: current_user.money + share.reward)

      # Add Transaction Snapshot
      transaction_snapshot = TransactionSnapshot.create(
        title: "Saldo Masuk, reward untuk share promosi/iklan!",
        description: "Berhasil, share promosi/iklan (<b>#{share.promotion.title}</b>) via <b>#{share.provider}</b> post id <b>#{share.post_id}</b> dengan reward <b>Rp.#{share.reward}</b>, maka pendapatan anda bertambah menjadi <b>Rp.#{get_nominal(current_user.money)}</b>", 
        transactionable_type: "#{share.class.name}", transactionable_id: "#{share.id}", user_id: current_user.id,
        credit: share.reward, balance: current_user.money, activity_snapshot_id: activity.id
      )
    end

    def post_tweet(added_data)
      client = Twitter::REST::Client.new do |c|
        c.consumer_key        = Rails.application.secrets.consumer_key
        c.consumer_secret     = Rails.application.secrets.consumer_secret
        c.access_token        = current_user.twitter_token
        c.access_token_secret = current_user.twitter_secret
      end

      tweet = added_data[:twit_message] + ' ' + added_data[:twit_url]
      @tweet_obj = client.update(tweet)
    end

    def check_available(promotion_id, provider)
      promotion = BuzzerShare.where(buzzer_id: current_user.id).where(promotion_id: promotion_id).where(provider: provider)
    end

    def check_quota(promotion_id)
      promotion = Promotion.find_by(id: promotion_id)
      promotion.status != Promotion::INACTIVE && promotion.check_quota?
    end
end

