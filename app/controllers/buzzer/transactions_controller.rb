class Buzzer::TransactionsController < Buzzer::BuzzerApplicationController
  def index
    @transactions = TransactionSnapshot.where(user_id: current_user.id).order("id desc").page(params[:page]).per(10)
  end
end