class Buzzer::CreditsController < Buzzer::BuzzerApplicationController
  def index
    @buzzer_shares = BuzzerShare.where(buzzer_id: current_user.id).order("id desc").page(params[:page]).per(10)
  end
end