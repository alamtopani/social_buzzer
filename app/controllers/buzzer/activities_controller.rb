class Buzzer::ActivitiesController < Buzzer::BuzzerApplicationController
  def index
    @activity_snapshots = current_user.activity_snapshots.order("id desc").page(params[:page]).per(10)
  end
end