class Buzzer::AccountsController < Buzzer::BuzzerApplicationController
  include PathHelper
  prepend_before_filter :draw_password, only: :update
  
  def edit
    @account = Buzzer.find current_user
  end

  def update
    @account = Buzzer.find current_user
    if @account.update(params_permit)
      redirect_to edit_account(current_user), notice: 'Successfully updated!'
    else
      redirect_to edit_account(current_user), alert: @account.errors.full_messages.join('<br/>')
    end
  end

  private
    def params_permit
      params.require(:buzzer).permit(:username, :email, :phone, :pin, :avatar, :password, :password_confirmation,
                                      buzzer_profile_attributes: [:first_name, :last_name,:born, :gender, :from, :agreement, :work, industries: []],
                                      address_attributes: [:country, :province, :city, :address, :longitude, :latitude]
                                    )
    end

    def draw_password
      %w(password password_confirmation).each do |attr|
        params[:buzzer].delete(attr)
      end if params[:buzzer] && params[:buzzer][:password].blank?
    end
end