class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  include PathHelper
  
  def facebook   
    init_omni_devise_callback('facebook')
  end

  def twitter
    init_omni_devise_callback('twitter')
  end

  def google_oauth2
    init_omni_devise_callback('google')
  end

  def instagram
    init_omni_devise_callback('instagram')
  end

  private
    def init_omni_devise_callback(provider)
      @user = User.find_for_devise_oauth(request.env["omniauth.auth"], current_user)
      case @user
      when :error
        message = 'Username atau Email Sudah di Gunakan Oleh Akun Lain!'
        service_redirect(message)
      when :error_current
        message = "Akun #{provider} anda sudah digunakan user lain, silahkan gunakan akun yang #{provider} berbeda!"
        rservice_redirect(message)
      when :error_different
        message = "Akun #{provider} yang saat ini login tidak sesuai dengan akun #{provider} yang anda daftarkan, silahkan login dengan akun #{provider} yang anda daftarkan pertama kali!"
        service_redirect(message)
      else
        if @user.present?
          sign_in_and_redirect current_user, :event => :authentication
          set_flash_message(:notice, :success, :kind => "#{provider}") if is_navigational_format?
        else
          session["devise.#{provider}_data"] = request.env["omniauth.auth"]
          redirect_to new_user_session_path
        end
      end
    end

    def service_redirect(message)
      if current_user.present?
        redirect_to request.referer || edit_account(current_user), alert: message 
      else
        redirect_to new_user_session_path, alert: message 
      end
    end
end