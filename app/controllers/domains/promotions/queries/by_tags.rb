module Promotion
  module Queries
    class ByTags
      def self.run(context_name)
        tags = ActsAsTaggableOn::Tagging.includes(:tag).where(context: context_name).distinct

        return tags
      end
    end
  end
end