function previewFile(input, imageHeader) {
  var preview = imageHeader[0];
  var file    = input.files[0];
  var reader  = new FileReader();

  reader.onloadend = function () {
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}

$(document).ready(function(){  
  $('.avatar-edit-account input').on('change', function(index){
    if($(this).length){
      previewFile(this, $('.avatar-edit-account img'));
    }
  });

  $('.yourCoverPromo').on('change', function(index){
    if($(this).length){
      previewFile(this, $('.yourCoverPromoPhoto'));
    }
  });

  $('#wizardControl a[href="#step3"]').on('shown.bs.tab', function (e) {
    google.maps.event.trigger(map, 'resize');
  });

  $('.select-province').on('change', function(){
    $.get('../../../xhrs/cities?id='+$(this).val()+'&node='+$(this).data('node'), function(result){
      $('.city-container').html(result);
      $('.city-container .select2').select2();
    });
  });
});