// -------------- shareToFacebook JS ---------------- //
window.fbAsyncInit = function() {
  FB.init({
    appId      : '411472995919462',
    xfbml      : true,
    version    : 'v2.9'
  });
  FB.AppEvents.logPageView();
};

(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "//connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));

function shareToFacebook() {
  var link = $("#the_link").val();
  FB.ui({
    method: "feed",
    link: link,
    name: $("#the_title").val(),
    description: $("#the_description").val(),
    picture: $("#the_picture").val(),
    quote: $("#the_message").val(),
    caption: $("#the_caption").val(),
  }, function(response){
    console.log(response);
    $.ajax({
      method: "POST",
      url: "/buzzer/buzzer_shares",
      data: {
        link: link,
        provider: 'facebook',
        promotion_id: $("#promotion_id").val(),
        post_id: response.post_id
      }
    }).done(function( msg ) {
      alert(msg.message);
      $('.btn-share-facebook').html('BERHASIL DI SHARE');
      $('.btn-share-facebook').css('background', '#ccc');
      $('.btn-share-facebook').css('border', 'solid 1px #bbbbbb');
      $('.btn-share-facebook').attr("onclick", null);
      // window.location = "/buzzer/jobs";
    });
  });
}

// function fillToForm(response, object) {
//   $("#" + object + "_facebook_access_token").val(response.authResponse.accessToken);
//   $("#" + object + "_facebook_expires_in").val(response.authResponse.expiresIn);
//   $("#" + object + "_facebook_signed_request").val(response.authResponse.signedRequest);
//   $("#" + object + "_facebook_user_id").val(response.authResponse.userID);
//   $(".facebook-button").html('<i aria-hidden="" class="fa fa-facebook-official"></i> Facebook <i class="fa fa-check" aria-hidden="true" style="color: rgb(31, 153, 217);"></i>')
// }

// function statusChangeCallback(response, object) {
//   if (response.status === 'connected') {
//     console.log(response);
//     fillToForm(response, object);
//   } else if (response.status === 'not_authorized') {
//     FB.login(function(response){
//       if (response.status === 'connected') {
//         console.log(response);
//         fillToForm(response, object);
//       }
//     });
//   }
// }

// function checkLoginState(object) {
//   FB.getLoginStatus(function(response) {
//     statusChangeCallback(response, object);
//   });
// }


// -------------- shareToTwitter JS ---------------- //
window.twttr = function(d, s, twitter_id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(twitter_id)) return t;
  js = d.createElement(s);
  js.id = twitter_id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);

  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };

  return t;
}(document, "script", "twitter-wjs");


function shareToTwitter() {
  $.ajax({
    method: "POST",
    url: "/buzzer/buzzer_shares",
    data: {
      provider: 'twitter',
      promotion_id: $("#promotion_id").val(),
      tweet: {
        twit_message: $("#the_content").val(),
        twit_url: $("#the_link").val()
      }
    }
  }).done(function( msg ) {
    alert(msg.message);
    $('.btn-share-twitter').html('BERHASIL DI SHARE');
    $('.btn-share-twitter').css('background', '#ccc');
    $('.btn-share-twitter').css('border', 'solid 1px #bbbbbb');
    $('.btn-share-twitter').attr("onclick", null);
    // window.location = "/buzzer/jobs";
  });
}

// var currCharNums;
// var TWEET_SIZE = 90;
// var TWEET_URL_SIZE = 23;

// function tweetCount(smth) {
//   nums = smth.length;
//   p = document.getElementById('tweet-count');

//   var urlPattern = /((http|https):\/\/)?([a-z0-9A-Z])+\.(com|co|[co\.id]|id|net|cc|tv|asia|org|me|gov|edu|club|info|live|today|online|company|news|guru|rocks|photography|fit|website|[net\.en]|tips|center|email|tips|vc|info|tech|world|biz|mobi|xyz|work|solutions|pub)+(\/([a-z0-9A-Z\S])*)*/g;
//   var urlCheck = smth.match(urlPattern);

//   if (nums > 0) {
//     nums = decreaseLength(nums, urlCheck);

//   } else {
//     nums = TWEET_SIZE;
//     urlNums = 0;
//   }

//   if (p != null) {
//     p.innerHTML = nums.toString();
//   }
// }

// function decreaseLength(nums, urlCheck) {
//   var urlLength = 0;
//   var urlNums = 0;

//   if(urlCheck!=null) {
//     urlNums = urlCheck.length;

//     for (var i=0; i<urlNums; i++) {
//       urlLength+=urlCheck[i].length;
//     }
//   }
//   return (TWEET_SIZE - (nums + urlNums * TWEET_URL_SIZE)) + urlLength;
// }