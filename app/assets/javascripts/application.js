// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require bootstrap.min
//= require jquery_ujs
//= require react
//= require react_ujs
//= require components
//= require angular
//= require angular-animate
//= require angular-resource
// require jquery.raty
// require ratyrate
//= require cocoon
// require ahoy
// require stick
//= require ckeditor/init
//= require ckeditor/config
//= require front/jquery-scrol-to-fixed
//= require front/underscore
//= require gmaps/google

function sideNavigation(){
  $('.button-collapse-nav').sideNav({
      menuWidth: 300, // Default is 300
      edge: 'right', // Choose the horizontal origin
      closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
      draggable: true // Choose whether you can drag to open on touch screens
    }
  );
}

function errorsMessages(){
  $('.alert .close').click(function(){
    $('.section-alert').hide();
  });
  $('.section-alert').click(function(){
    $('.section-alert').hide();
  });
}

function sidePanelFixed(){
  var getWidth = $('.s3').width();
  $('.fixed-panel').width(getWidth);
}

function previewFile(input, imageHeader) {
  var preview = imageHeader[0];
  var file    = input.files[0];
  var reader  = new FileReader();

  reader.onloadend = function () {
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}

function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -6.173664985778263, lng: 106.82918787002563},
    zoom: 13
  });

  var input = document.getElementById('pac-input');

  var autocomplete = new google.maps.places.Autocomplete(input, {placeIdOnly: true});
  autocomplete.bindTo('bounds', map);

  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  var infowindow = new google.maps.InfoWindow();
  var infowindowContent = document.getElementById('infowindow-content');
  infowindow.setContent(infowindowContent);
  var geocoder = new google.maps.Geocoder;
  var marker = new google.maps.Marker({
    map: map
  });

  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });

  google.maps.event.addListener(map, 'click', function(event) {
    marker.setPosition(event.latLng);
    var latLong = event.latLng;
    latitude = $('.latitude').val(latLong.lat());
    longitude = $('.longitude').val(latLong.lng());
  });


  autocomplete.addListener('place_changed', function() {
    infowindow.close();
    var place = autocomplete.getPlace();

    if (!place.place_id) {
      return;
    }
    geocoder.geocode({'placeId': place.place_id}, function(results, status) {
      if (status !== 'OK') {
        window.alert('Geocoder failed due to: ' + status);
        return;
      }
      map.setZoom(13);
      map.setCenter(results[0].geometry.location);

      // Set the position of the marker using the place ID and location.
      // marker.setPlace({
      //   placeId: place.place_id,
      //   location: results[0].geometry.location
      // });

      // marker.setVisible(true);
      // infowindowContent.children['place-name'].textContent = place.name;
      // infowindowContent.children['place-id'].textContent = place.place_id;
      // infowindowContent.children['place-address'].textContent = results[0].formatted_address;
      // infowindow.open(map, marker);

      google.maps.event.addListener(map, 'click', function(event) {
        marker.setPosition(event.latLng);
        var latLong = event.latLng;
        latitude = $('.latitude').val(latLong.lat());
        longitude = $('.longitude').val(latLong.lng());
      });
    });
  });
}


$(document).ready(function(){
  $('nav').scrollToFixed();
  sideNavigation();
  errorsMessages();
  sidePanelFixed();
  $('#modal_devise_error').show();
  $('.scrollspy').scrollSpy();
  $('select').material_select();
  $('.avatar-edit-account input').on('change', function(index){
    if($(this).length){
      previewFile(this, $('.avatar-edit-account img'));
    }
  });

  $('.tabs-fixed-width a[href="#edit-profile"]').on('click', function () {
    setTimeout(function () {
      google.maps.event.trigger(map, 'resize');
    }, 10);
  });
});
