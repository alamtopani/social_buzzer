class BusinessInfo < ApplicationRecord
  belongs_to :user, foreign_key: 'business_id'

  def get_industries
    industries = self.industries.reject(&:empty?)
    return industries.join(',')
  end
end
