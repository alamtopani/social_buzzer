class TransactionSnapshot
  include Mongoid::Document

  field :title, type: String
  field :transactionable_type, type: String
  field :transactionable_id, type: String
  field :activity_snapshot_id, type: String
  field :user_id, type: Integer
  field :code, type: String
  field :description, type: String
  field :debit, type: Integer, default: 0
  field :credit, type: Integer, default: 0
  field :balance, type: Integer, default: 0
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  before_create :before_create_service
  before_save :before_update_service

  # ------- RELATIONSHIP --------
  def user
    user = User.find_by(id: self.user_id)
    return user
  end

  def transactionable
    if self.activitiable_type == 'BuzzerShare'
      buzzer_share = BuzzerShare.find_by(id: self.activitiable_id)
      return buzzer_share
    end
  end

  def activity_snapshot
    if self.activitiable_type == 'BuzzerShare'
      activity_snapshot = ActivitySnapshot.find_by(id: self.activitiable_id)
      return activity_snapshot
    end
  end

  def reward?
    if self.credit > 0
      @klass = 'success'
      @reward = self.credit
      @icon = 'up'
    elsif self.debit > 0
      @klass = 'default'
      @reward = self.debit
      @icon = 'down'
    end

    return "<span class='text-#{@klass}'>Rp.#{@reward}</span> <i class=\"fa fa-level-#{@icon} text-#{@klass}\"></i>".html_safe
  end

  private
    def before_create_service
      self.code = 'TR'+ 5.times.map{Random.rand(10)}.join
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
