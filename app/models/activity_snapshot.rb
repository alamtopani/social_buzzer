class ActivitySnapshot
  include Mongoid::Document
  field :title, type: String
  field :activitiable_type, type: String
  field :activitiable_id, type: String
  field :user_id, type: Integer
  field :code, type: String
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  before_create :before_create_service
  before_save :before_update_service

  BUZZERSHARE = 'BuzzerShare'.freeze

  STATUSES = [self::BUZZERSHARE].freeze
  DEFAULT_STATUSES = [self::BUZZERSHARE].freeze

  scope :statuses, ->{where(activitiable_type: { '$in': [self::BUZZERSHARE] }) }

  # ------- RELATIONSHIP --------
  def user
    user = User.find_by(id: self.user_id)
    return user
  end

  def activitiable
    if self.activitiable_type == 'BuzzerShare'
      buzzer_share = BuzzerShare.find_by(id: self.activitiable_id)
      return buzzer_share
    end
  end

  def transaction_snapshot
    if self.activitiable_type == 'BuzzerShare'
      transaction_snapshot = TransactionSnapshot.find_by(activity_snapshot_id: self.id)
      return transaction_snapshot
    end
  end

  def subject?
    if self.activitiable_type == 'BuzzerShare'
      buzzer_share = 'Sukses share promosi/iklan'
      return buzzer_share
    end
  end

  private
    def before_create_service
      self.code = 'AC'+ 5.times.map{Random.rand(10)}.join
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end

end
