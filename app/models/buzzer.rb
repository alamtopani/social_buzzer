class Buzzer < User
  default_scope {where(type: 'Buzzer')}
  has_one :buzzer_profile, foreign_key: 'buzzer_id'
  accepts_nested_attributes_for :buzzer_profile, allow_destroy: true
  
  after_initialize :prepare_profile

  # ------- RELATIONSHIP --------
  def buzzer_shares
    buzzer_shares = BuzzerShare.where(buzzer_id: self.id)
    return buzzer_shares
  end

  # ------- ATTRIBUTES --------
  def fullname
    self.buzzer_profile.first_name + ' ' + self.buzzer_profile.last_name if self.buzzer_profile.first_name?
  end

  def get_industries
    self.buzzer_profile.industries.reject(&:empty?).join(', ') if self.buzzer_profile.present?
  end

  def get_progress_profile_buzzer
    progress = 17

    progress = progress + 7 if self.avatar.present?
    progress = progress + 7 if self.phone.present?

    progress = progress + 6 if self.twitter_uid.present?
    progress = progress + 6 if self.facebook_uid.present?
    progress = progress + 6 if self.instagram_uid.present?
    progress = progress + 5 if self.google_uid.present?
    progress = progress + 5 if self.youtube_uid.present?

    if self.buzzer_profile.present?
      progress = progress + 5 if self.buzzer_profile.first_name.present?
      progress = progress + 5 if self.buzzer_profile.last_name.present?
      progress = progress + 5 if self.buzzer_profile.born.present?
      progress = progress + 5 if self.buzzer_profile.gender.present?
      progress = progress + 5 if self.buzzer_profile.work.present?
      progress = progress + 5 if self.buzzer_profile.from.present?
      progress = progress + 5 if self.buzzer_profile.industries.present?
    end

    if self.address.present?
      progress = progress + 5 if self.address.province.present?
      progress = progress + 5 if self.address.city.present?
      progress = progress + 5 if self.address.address.present?
      progress = progress + 3.5 if self.address.longitude.present?
      progress = progress + 3.5 if self.address.latitude.present?
    end

    return progress
  end

  private
    def prepare_profile
      self.buzzer_profile = BuzzerProfile.new if self.buzzer_profile.blank?
    end
end