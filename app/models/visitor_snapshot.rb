class VisitorSnapshot < ApplicationRecord
  is_impressionable
  has_many :impressions, as: :impressionable
  
  def visitorable
    if self.visitorable_type == 'Promotion'
      visitorable = Promotion.find(self.visitorable_id)
    end
  end

  def visitor_count_day
    today = Time.zone.now
    visitors = self.impressions.where("DATE(created_at) = DATE(?)", Time.zone.now).count
  end

  def visitor_count_week
    today = Time.zone.now
    visitors = self.impressions.where("DATE(created_at) >= DATE(?) AND DATE(created_at) <= DATE(?)", today.at_beginning_of_week, today.at_end_of_week).count
  end

  def visitor_count_month
    today = Time.zone.now
    visitors = self.impressions.where("DATE(created_at) >= DATE(?) AND DATE(created_at) <= DATE(?)", today.at_beginning_of_month, today.at_end_of_month).count
  end

  def visitor_count_year
    today = Time.zone.now
    visitors = self.impressions.where("DATE(created_at) >= DATE(?) AND DATE(created_at) <= DATE(?)", today.beginning_of_year, today.end_of_year).count
  end

  def visitor_per_year
    today = Time.zone.now
    visitors = self.impressions.where("DATE(created_at) >= DATE(?) AND DATE(created_at) <= DATE(?)", today.beginning_of_year, today.end_of_year)
  end

  def visitor_per_month
    today = Time.zone.now
    visitors = self.impressions.where("DATE(created_at) >= DATE(?) AND DATE(created_at) <= DATE(?)", today.at_beginning_of_month, today.at_end_of_month)
  end
end
