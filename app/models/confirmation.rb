class Confirmation
  include Mongoid::Document
  field :user_id, type: Integer
  field :invoice_code, type: String
  field :name, type: String
  field :email, type: String
  field :payment_date, type: Date
  field :price, type: Integer
  field :bank_account, type: String
  field :payment_method, type: String
  field :sender_name, type: String
  field :message, type: String
  field :category, type: String
  field :status, type: String
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  before_create :before_create_service
  before_save :before_update_service

  # ------- STATUS --------
  PAID = 'paid'.freeze
  UNPAID = 'unpaid'.freeze

  STATUSES = [self::PAID, self::UNPAID].freeze

  def status?
    if self.status == true
      return "<label class='label label-success'>#{Confirmation::PAID.titleize}</label>".html_safe
    else
      return "<label class='label label-danger'>#{Confirmation::UNPAID.titleize}</label>".html_safe
    end
  end

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
