class BuzzerProfile < ApplicationRecord
  belongs_to :user, foreign_key: 'buzzer_id'

  attr_reader :age

  def age
    if self.born.present?
      (Date.today - self.born.to_date).to_i / 365
    end
  end
end
