class Business < User
  default_scope {where(type: 'Business')}
  has_one :business_info, foreign_key: 'business_id'
  accepts_nested_attributes_for :business_info, allow_destroy: true

  after_initialize :prepare_profile

  # ------- RELATIONSHIP --------
  def promotions
    promotions = Promotion.where(user_id: self.id)
    return promotions
  end

  # ------- ATTRIBUTES --------
  def get_progress_profile_business
    progress = 15

    progress = progress + 10 if self.avatar.present?

    if self.business_info.present?
      progress = progress + 7 if self.business_info.name.present?
      progress = progress + 7 if self.business_info.website.present?
      progress = progress + 7 if self.business_info.admin_name.present?
      progress = progress + 7 if self.business_info.phone_admin.present?
      progress = progress + 7 if self.business_info.about.present?
      progress = progress + 7 if self.business_info.from.present?
      progress = progress + 7 if self.business_info.industries.present?
    end

    if self.address.present?
      progress = progress + 7 if self.address.province.present?
      progress = progress + 7 if self.address.city.present?
      progress = progress + 7 if self.address.address.present?
      progress = progress + 2.5 if self.address.longitude.present?
      progress = progress + 2.5 if self.address.latitude.present?
    end

    return progress
  end

  private
    def prepare_profile
      self.business_info = BusinessInfo.new if self.business_info.blank?
    end
end