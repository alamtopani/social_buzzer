class BuzzerShare
  include Mongoid::Document
  field :promotion_id, type: String
  field :buzzer_id, type: Integer
  field :post_id, type: Integer
  field :post_link, type: String
  field :reward, type: Integer
  field :status, type: String, default: 'pending'
  field :provider, type: String
  field :count_engagement, type: Integer, default: 0
  field :count_comment, type: Integer, default: 0
  field :count_share, type: Integer, default: 0
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  scope :successed, ->{where(status: { '$in': [self::SUCCESS, self::PENDING] }) }
  scope :only_successed, ->{where(status: self::SUCCESS) }
  scope :rejected, ->{where(status: self::REJECT) }

  before_create :before_create_service
  before_save :before_update_service

  # ------- RELATIONSHIP --------
  def buzzer
    buzzer = Buzzer.find_by(id: self.buzzer_id)
    return buzzer
  end

  def promotion
    promo = Promotion.find_by(id: self.promotion_id)
    return promo
  end

  # belongs_to :buzzer
  # belongs_to :promotion

  # ------- STATUS --------
  SUCCESS = 'success'.freeze
  REJECT = 'reject'.freeze
  PENDING = 'pending'.freeze

  STATUSES = [self::SUCCESS, self::REJECT, self::PENDING].freeze

  def status?
    case self.status
    when BuzzerShare::SUCCESS then @klass = 'success'
    when BuzzerShare::REJECT then @klass = 'danger'
    when BuzzerShare::PENDING   then @klass = 'default'
    end

    return "<span class=\"label label-#{@klass}\">#{self.status.titleize}</span>".html_safe
  end

  def status_reward?
    case self.status
    when BuzzerShare::SUCCESS then @klass = 'success'
    when BuzzerShare::REJECT then @klass = 'danger'
    when BuzzerShare::PENDING   then @klass = 'default'
    end

    return "<span class='text-#{@klass}'>Rp.#{self.reward}</span> <i class=\"fa fa-level-up text-#{@klass}\"></i>".html_safe
  end

  def count_engagement?    
    if self.provider == 'facebook'
      prepare_graph_facebook
      @result = @response[:og_object][:engagement][:count] rescue 0
    else
      return @result = 'Tidak tersedia'
    end

    if @result == self.count_engagement
      return self.count_engagement
    else
      self.update(count_engagement: @result)
      return @result
    end
  end

  def count_comment?
    if self.provider == 'facebook'
      prepare_graph_facebook
      @result = @response[:share][:comment_count] rescue 0
    else
      return @result = 'Tidak tersedia'
    end
    
    if @result == self.count_comment
      return self.count_comment
    else
      self.update(count_comment: @result)
      return @result
    end
  end

  def count_share?   
    if self.provider == 'facebook'
      prepare_graph_facebook
      @result = @response[:share][:share_count] rescue 0
    else
      return @result = 'Tidak tersedia'
    end
    
    if @result == self.count_share
      return self.count_share
    else
      self.update(count_share: @result)
      return @result
    end
  end

  def prepare_graph_facebook
    require 'rest-client'

    @url_graph = "https://graph.facebook.com/v2.7?emc=rss&fields=og_object{engagement},share&access_token=#{self.buzzer.facebook_oauth_token}&id=#{self.post_link}"
    
    if self.post_link.present?
      @response = RestClient.get(@url_graph)
      @response = eval(@response)
    else
      @response = nil
    end
  end

  def before_create_service
    self.reward = Rails.application.secrets.share_reward
    self.created_at = Time.zone.now
    self.updated_at = Time.zone.now
  end

  def before_update_service
    self.updated_at = Time.zone.now
  end
end
