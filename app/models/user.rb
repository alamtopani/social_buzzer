class User < ApplicationRecord
  extend FriendlyId
  friendly_id :username, use: [:slugged, :finders]

  self.inheritance_column = nil
  
  devise  :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable,
          :validatable, :lockable, :confirmable, :omniauthable, omniauth_providers: [:facebook, :google_oauth2, :twitter, :instagram], :authentication_keys => [:login]

  after_initialize :prepare_address
  before_create :prepare_id

  mount_uploader :avatar, PhotoUploader
  mount_uploader :twitter_avatar, PhotoSocialMediaUploader
  mount_uploader :facebook_avatar, PhotoSocialMediaUploader
  mount_uploader :instagram_avatar, PhotoSocialMediaUploader
  mount_uploader :google_avatar, PhotoSocialMediaUploader
  mount_uploader :youtube_avatar, PhotoSocialMediaUploader

  # SCOPE CONDITION
  today = Time.zone.now
  scope :count_today, ->{where("DATE(created_at) = DATE(?)", Time.zone.now).count}
  scope :count_week, ->{where("DATE(created_at) >= DATE(?) AND DATE(created_at) <= DATE(?)", today.at_beginning_of_week, today.at_end_of_week).count}
  scope :count_month, ->{where("DATE(created_at) >= DATE(?) AND DATE(created_at) <= DATE(?)", today.at_beginning_of_month, today.at_end_of_month).count}
  scope :count_year, ->{where("DATE(created_at) >= DATE(?) AND DATE(created_at) <= DATE(?)", today.beginning_of_year, today.end_of_year).count}
  scope :per_year, ->{where("DATE(created_at) >= DATE(?) AND DATE(created_at) <= DATE(?)", today.beginning_of_year, today.end_of_year)}
  scope :per_month, ->{where("DATE(created_at) >= DATE(?) AND DATE(created_at) <= DATE(?)", today.at_beginning_of_month, today.at_end_of_month)}

  # ------- RELATIONSHIP --------
  has_one :address, foreign_key: 'user_id'
  accepts_nested_attributes_for :address, allow_destroy: true

  def activity_snapshots
    activity_snapshots = ActivitySnapshot.where(user_id: self.id)
    return activity_snapshots
  end

  include UserModule::UserAuthenticate

  # ------- TYPE --------
  def is_buzzer?
    self.type == 'Buzzer'
  end

  def is_business?
    self.type == 'Business'
  end

  def is_admin?
    self.type == 'Admin'
  end

  # ------- ATTRIBUTES --------
  def get_address_lat
    self.address.present? && self.address.latitude ? self.address.latitude : -6.173664985778263
  end

  def get_address_long
    self.address.present? && self.address.longitude ? self.address.longitude : 106.82918787002563
  end

  private
    def prepare_id
      self.uid = 6.times.map{ Random.rand(10000000000)}.join
      self.referral_code = 'B' + 6.times.map{ Random.rand(10)}.join
    end

    def prepare_address
      self.address = Address.new if self.address.blank?
    end
end
