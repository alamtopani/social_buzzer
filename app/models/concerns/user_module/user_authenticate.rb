module UserModule
  module UserAuthenticate
    extend ActiveSupport::Concern
    included do
      attr_accessor :login

      def authenticate(login, password)
        user = where("username = :login OR email = :login", { login: login }).first
        return nil unless user
        return nil unless user.valid_password?(password)
        user
      end

      def self.find_for_database_authentication(warden_conditions)
        conditions = warden_conditions.dup
        if login = conditions.delete(:login)
          where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
        else
          where(conditions).first
        end
      end

      def login=(login)
        @login = login
      end

      def login
        @login || self.username || self.email
      end

      def self.find_for_devise_oauth(auth_hash, signed_in_resource)
        if auth_hash.provider == 'instagram'
          check_email = auth_hash.info.email ? auth_hash.info.email : auth_hash.info.nickname.downcase
        else  
          check_email = auth_hash.info.email ? auth_hash.info.email : auth_hash.extra.raw_info.name.downcase
        end

        if auth_hash.provider == 'facebook'
          @buzzer = User.where("users.facebook_uid =? OR users.email =?", auth_hash.uid, check_email).last 

          if signed_in_resource.present?
            buzzer = signed_in_resource

            if @buzzer.blank? || @buzzer == signed_in_resource
              if buzzer.facebook_uid.blank? || buzzer.facebook_uid == auth_hash.uid  
                buzzer.prepare_attr_devise('facebook', signed_in_resource, auth_hash, check_email)
              elsif buzzer.facebook_uid != auth_hash.uid
                return :error_different
              end
            elsif @buzzer.present? && @buzzer.facebook_uid != buzzer.facebook_uid
              return :error_current
            else
              return :error
            end

            return signed_in_resource
          elsif @buzzer.blank?
            check_user = User.where("users.email =? OR users.username =?", check_email, auth_hash.extra.raw_info.name.downcase).last

            if check_user.present?
              return :error
            else
              buzzer = self.find_or_create_by(provider: auth_hash.provider, facebook_uid: auth_hash.uid)
              buzzer = buzzer.prepare_attr_devise('facebook', signed_in_resource, auth_hash, check_email)
              return buzzer
            end
          end

        elsif auth_hash.provider == 'twitter'
          @buzzer = User.where("users.twitter_uid =? OR users.email =?", auth_hash.uid, check_email).last 

          if signed_in_resource.present?
            buzzer = signed_in_resource

            if @buzzer.blank? || @buzzer == signed_in_resource
              if buzzer.twitter_uid.blank? || buzzer.twitter_uid == auth_hash.uid
                buzzer.prepare_attr_devise('twitter', signed_in_resource, auth_hash, check_email)            
              elsif buzzer.twitter_uid != auth_hash.uid
                return :error_different
              end
            elsif @buzzer.present? && @buzzer.twitter_uid != buzzer.twitter_uid
              return :error_current
            else
              return :error
            end

            return signed_in_resource
          elsif @buzzer.blank?
            check_user = User.where("users.email =? OR users.username =?", check_email, auth_hash.extra.raw_info.name.downcase).last

            if check_user.present?
              return :error
            else
              buzzer = self.find_or_create_by(provider: auth_hash.provider, twitter_uid: auth_hash.uid)
              buzzer = buzzer.prepare_attr_devise('twitter', signed_in_resource, auth_hash, check_email)
              return buzzer
            end
          end

        elsif auth_hash.provider == 'instagram'
          @buzzer = User.where("users.instagram_uid =? OR users.email =?", auth_hash.uid, check_email).last 

          if signed_in_resource.present?
            buzzer = signed_in_resource

            if @buzzer.blank? || @buzzer == signed_in_resource
              if buzzer.instagram_uid.blank? || buzzer.instagram_uid == auth_hash.uid  
                buzzer.prepare_attr_devise('instagram', signed_in_resource, auth_hash, check_email)            
              elsif buzzer.instagram_uid != auth_hash.uid
                return :error_different
              end
            elsif @buzzer.present? && @buzzer.instagram_uid != buzzer.instagram_uid
              return :error_current
            else
              return :error
            end

            return signed_in_resource
          elsif @buzzer.blank?
            check_user = User.where("users.email =? OR users.username =?", check_email, auth_hash.extra.raw_info.name.downcase).last

            if check_user.present?
              return :error
            else
              buzzer = self.find_or_create_by(provider: auth_hash.provider, instagram_uid: auth_hash.uid)
              buzzer = buzzer.prepare_attr_devise('instagram', signed_in_resource, auth_hash, check_email)
              return buzzer
            end
          end

        elsif auth_hash.provider == 'google_oauth2'
          @buzzer = User.where("users.google_uid =? OR users.email =?", auth_hash.uid, check_email).last 

          if signed_in_resource.present?
            buzzer = signed_in_resource

            if @buzzer.blank? || @buzzer == signed_in_resource
              if buzzer.google_uid.blank? || buzzer.google_uid == auth_hash.uid  
                buzzer.prepare_attr_devise('google_oauth2', signed_in_resource, auth_hash, check_email)            
              elsif buzzer.google_uid != auth_hash.uid
                return :error_different
              end
            elsif @buzzer.present? && @buzzer.google_uid != buzzer.google_uid
              return :error_current
            else
              return :error
            end

            return signed_in_resource
          elsif @buzzer.blank?
            check_user = User.where("users.email =? OR users.username =?", check_email, auth_hash.extra.raw_info.name.downcase).last

            if check_user.present?
              return :error
            else
              buzzer = self.find_or_create_by(provider: auth_hash.provider, google_uid: auth_hash.uid)
              buzzer = buzzer.prepare_attr_devise('google_oauth2', signed_in_resource, auth_hash, check_email)
              return buzzer
            end
          end

        end

      end


      ################ CHECK ATTR DEVISE OUTH ################

      def prepare_attr_devise(provider, current_user, auth_hash, check_email)
        unless current_user.present?
          self.username = auth_hash.extra.raw_info.name.downcase
          self.provider = auth_hash.provider
          self.email = check_email
          self.password = Devise.friendly_token[0,10]
          self.confirmation_token = nil
          self.confirmed_at = Time.now
          self.type = 'Buzzer'
        end

        if provider == 'facebook'
          self.facebook_uid = auth_hash.uid
          self.facebook_oauth_token = auth_hash.credentials.token
          self.facebook_oauth_token_expires = Time.at(auth_hash.credentials.expires_at).in_time_zone
          self.facebook_oauth_token_register_at = Time.now.in_time_zone
          self.remote_facebook_avatar_url = auth_hash.info.image.gsub('http://','https://') if self.facebook_avatar.blank?
          self.remote_avatar_url = auth_hash.info.image.gsub('http://','https://') if self.avatar.blank?
          self.save!
          self.facebook_check_token_expires(auth_hash.credentials.token, auth_hash.credentials.expires_at)
          self.update_facebook_friends()
        elsif provider == 'twitter'
          self.twitter_uid = auth_hash.uid
          self.twitter_token = auth_hash.credentials.token
          self.twitter_secret = auth_hash.credentials.secret
          self.twitter_oauth_token_register_at = Time.now.in_time_zone
          self.remote_instagram_avatar_url = auth_hash.info.image.gsub('http://','https://') if self.instagram_avatar.blank?
          self.remote_avatar_url = auth_hash.info.image.gsub('http://','https://') if self.avatar.blank?
          self.save!
          self.update_twitter_followers(auth_hash)    
        elsif provider = 'instagram'
          self.instagram_uid = auth_hash.uid
          self.instagram_oauth_token = auth_hash.credentials.token
          self.instagram_oauth_token_register_at = Time.now.in_time_zone
          self.save!
          # buzzer.update_instagram_followers(auth_hash)   
        elsif provider = 'google_oauth2'
          self.google_uid = auth_hash.uid
          self.google_oauth_token = auth_hash.credentials.token
          self.google_oauth_token_expires = Time.at(auth_hash.credentials.expires_at).in_time_zone
          self.google_oauth_token_register_at = Time.now.in_time_zone
          self.save!
          self.google_check_token_expires(auth_hash.credentials.token, auth_hash.credentials.expires_at)
        end
      end


      ################ CHECK FRIENDS AND FOLLOWERS ################

      def update_friends_followers(auth_hash)
        if auth_hash.provider == 'twitter'
          self.update_twitter_followers(auth_hash)
        else auth_hash.provider == 'facebook'
          self.facebook_check_token_expires(auth_hash.credentials.token, auth_hash.credentials.expires_at)
          self.update_facebook_friends()
        end
      end

      def update_facebook_friends()
        graph = Koala::Facebook::API.new(self.facebook_oauth_token)
        self.facebook_friends_count = graph.get_connection("me", "friends", api_version:"v2.0").raw_response["summary"]["total_count"]
        self.save
      end

      def update_twitter_followers(auth_hash)
        if !self.twitter_uid.present?
          self.twitter_uid = auth_hash.uid
          self.twitter_token = auth_hash.credentials.token
          self.twitter_secret = auth_hash.credentials.secret
        end
        self.twitter_followers_count = auth_hash.extra.raw_info.followers_count
        self.save
      end

      def update_instagram_followers(auth_hash)
        if !self.instagram_uid.present?
          self.instagram_uid = auth_hash.uid
          self.instagram_oauth_token = auth_hash.credentials.token
          # self.instagram_secret = auth_hash.credentials.secret
        end
        self.instagram_followers_count = auth_hash.extra.raw_info.followers_count
        self.save
      end


      ################ CHECK EXPIRED TOKEN ################

      def facebook_check_token_expires(token, token_expires)
        if self.facebook_oauth_token.nil? || (Time.now.in_time_zone >= self.facebook_oauth_token_expires)
          self.facebook_oauth_token = token
          self.facebook_oauth_token_expires = Time.at(token_expires).in_time_zone
        end
      end

      def google_check_token_expires(token, token_expires)
        if Time.zone.now >= self.google_oauth_token_expires
          self.google_oauth_token = token
          self.google_oauth_token_expires = Time.at(token_expires).in_time_zone
        end
      end
      
    end
  end
end