class Address < ApplicationRecord
  belongs_to :user, foreign_key: 'user_id'

  def sort_place_info
    [country, province, city].select(&:'present?').join(', ')
  end

  def place_info
    [country, province, city, address].select(&:'present?').join(', ')
  end
end
