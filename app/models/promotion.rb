class Promotion
  include Mongoid::Document
  field :code, type: String
  field :title, type: String
  field :cover, type: String
  field :description, type: String
  field :content, type: String
  field :url, type: Array
  field :mention, type: Array
  field :hashtag, type: Array
  field :cities, type: Array
  field :range_age, type: String
  field :min_age, type: Integer
  field :max_age, type: Integer
  field :gender, type: String
  field :industries, type: Array
  field :providers, type: Array
  field :status, type: String, default: 'inactive'
  field :quota, type: Integer
  field :quota_share, type: Integer
  field :price, type: Integer
  field :fee, type: Integer
  field :total, type: Integer
  field :address, type: String
  field :longitude, type: String
  field :latitude, type: String
  field :url_facebook, type: String
  field :url_twitter, type: String
  field :url_instagram, type: String
  field :phone, type: String
  field :start_date, type: Date
  field :expired_date, type: Date
  field :created_at, type: DateTime
  field :updated_at, type: DateTime
  field :user_id, type: Integer

  mount_uploader :cover, CoverUploader

  scope :all_active, ->{where(status: { '$in': [self::HEADLINE, self::FEATURED, self::ACTIVE] }) }

  before_create :before_create_service
  before_save :before_update_service

  # ------- RELATIONSHIP --------
  def business
    business = Business.find_by(id: self.user_id)
    return business
  end

  def buzzer_shares
    buzzer_shares = BuzzerShare.where(promotion_id: self.id)
    return buzzer_shares
  end

  def visitor_snapshot
    visitor_snapshot = VisitorSnapshot.where(visitorable_type: self.class.name, visitorable_id: self.id.to_s).last || nil
    return visitor_snapshot
  end

  # ------- PACKAGE --------
  BASIC = 'basic'.freeze
  STANDAR = 'standar'.freeze
  PREMIUM = 'premium'.freeze
  PRESTIGE = 'prestige'.freeze

  # ------- STATUS --------
  HEADLINE = 'headline'.freeze
  FEATURED = 'featured'.freeze
  ACTIVE = 'active'.freeze
  INACTIVE = 'inactive'.freeze

  STATUSES = [self::INACTIVE, self::ACTIVE, self::HEADLINE, self::FEATURED].freeze
  DEFAULT_STATUSES = [self::ACTIVE, self::HEADLINE, self::FEATURED].freeze

  def status?
    case self.status
    when Promotion::HEADLINE then @klass = 'warning'
    when Promotion::FEATURED then @klass = 'success'
    when Promotion::ACTIVE   then @klass = 'info'
    when Promotion::INACTIVE then @klass = 'default'
    end

    return "<span class=\"label label-#{@klass}\">#{self.status.titleize}</span>".html_safe
  end

  def status_payment?
    if self.status == Promotion::INACTIVE
      return "<span class=\"label label-danger\">PENDING</span>".html_safe
    else
      return "<span class=\"label label-success\">SUDAH DIBAYAR</span>".html_safe
    end
  end

  # ------- ATTRIBUTES --------
  def get_address_lat
    self.latitude.present? ? self.latitude : -6.173664985778263
  end

  def get_address_long
    self.longitude.present? ? self.longitude : 106.82918787002563
  end

  def budget?
    budget = self.price.to_i - self.buzzer_shares.successed.sum(:reward)
  end

  def check_quota?
    self.buzzer_shares.successed.size.to_i < self.quota.to_i
  end

  def quota?
    count = self.quota.to_i - self.buzzer_shares.successed.size.to_i
  end

  def content_caption?
    message = self.hashtag.join(' ') + self.mention.join(' ')
  end

  def content_promo?
    message = self.hashtag.join(' ') + self.mention.join(' ') + self.content
  end

  def already_share(provider, current_user)
    self.buzzer_shares.where(provider: provider).where(buzzer_id: current_user.id).where(promotion_id: self.id)
  end

  def duration?
    if self.start_date.to_date == self.expired_date.to_date
      return self.start_date.strftime('%d %B %Y')
    elsif self.start_date.strftime('%B') == self.expired_date.strftime('%B')
      return "#{self.start_date.strftime('%d')} - #{self.expired_date.strftime('%d %b %Y')}"
    else
      return "#{self.start_date.strftime('%d %b')} - #{self.expired_date.strftime('%d %b %Y')}"
    end
  end

  def campaign_encript(current_user)
    campaign_encript = current_user.id.to_s + current_user.username + current_user.email + self.id
    return Digest::SHA1.hexdigest(campaign_encript)
  end

  def is_facebook_only?
    self.providers.include?('Facebook')
  end

  def is_twitter_only?
    self.providers.include?('Twitter')
  end

  private
    def before_create_service
      self.quota_share = self.quota
      self.code = 'P'+ 5.times.map{Random.rand(10)}.join
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end