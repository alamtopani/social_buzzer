class PhotoSocialMediaUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  storage :file

  def store_dir
    "uploads/user/social_media/#{mounted_as}/#{model.id}"
  end

  def default_url(*args)
    ActionController::Base.helpers.asset_path("user_default.jpeg")
  end

  version :thumb do
    process :resize_to_fit => [170, 170]
  end

  def extension_white_list
    %w(jpg jpeg png gif)
  end
end
